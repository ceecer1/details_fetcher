package co.home.intel.util

import java.time.format.DateTimeFormatter
import java.time.{Instant, LocalDateTime, ZoneOffset}
import java.util.UUID

import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.joda.time.LocalDate
import org.json4s
import org.json4s.JsonAST.{JInt, JNull, JString}
import org.json4s.{CustomSerializer, DefaultFormats, Formats, NoTypeHints, Serialization}


/**
  * json4s json serialisation
  */
trait JsonSerialization extends Json4sSupport {


  protected val dateTimeFormat = DateTimeFormatter.ISO_DATE_TIME
  protected val dateTimeSerializer = new CustomSerializer[Instant](_ => ({
    case JString(s) => LocalDateTime.parse(s, dateTimeFormat).toInstant(ZoneOffset.UTC);
  }, {
    case d: Instant => JString(dateTimeFormat.format(LocalDateTime.ofInstant(d, ZoneOffset.UTC)))
  }))

  implicit val formats: Formats = DefaultFormats ++ List(JInstantSerializer) ++ List(UUIDSerialiser) + dateTimeSerializer
  implicit val serialization: Serialization = json4s.native.Serialization


}



case object JInstantSerializer extends CustomSerializer[Instant]( _ => (
  {
    case JInt(d) => Instant.ofEpochMilli(d.toLong)
    case JNull => null
  },
  {
    case i: Instant => JInt(i.toEpochMilli)
  }
))

case object UUIDSerialiser extends CustomSerializer[UUID](format => (
  {
    case JString(s) => UUID.fromString(s)
    case JNull => null
  },
  {
    case x: UUID => JString(x.toString)
  }
)
)