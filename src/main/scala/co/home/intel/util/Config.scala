package co.home.intel.util

import com.typesafe.config.ConfigFactory

/**
  * Created by shishir on 2/4/17.
  */
object Config {

  val config = ConfigFactory.load()

  case object ServerConfig {
    private val serverConfig = config.getConfig("server")
    lazy val systemName = serverConfig.getString("system-name")
    lazy val port = serverConfig.getInt("port")
    lazy val interface = serverConfig.getString("interface")
    lazy val defaultTimeout = serverConfig.getInt("request-timeout")
  }

  case object ApiUrlConfig {
    lazy val challengeURL = config.getString("challenge-url")
    lazy val upcomingURL = config.getString("upcoming-url")
    lazy val pastURL = config.getString("past-url")
    lazy val duration = config.getInt("poll-duration")
  }

}
