package co.home.intel.util

import doobie._
import cats.effect.IO
import SystemConfig.PostgresqlConfig

/**
  * Doobie transaction
  */
trait TransactionProvider {

  implicit val xa: Transactor.Aux[IO, Unit]

}

trait DefaultTransactionProvider extends TransactionProvider {

  override implicit val xa = Transactor.fromDriverManager[IO]("org.postgresql.Driver",
    PostgresqlConfig.rdsurl, PostgresqlConfig.rdsusername, PostgresqlConfig.rdspassword)

}

