package co.home.intel.util

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.util.Timeout
import co.home.intel.util.Config.ServerConfig
import Config.{ServerConfig, config}

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext

trait AkkaServiceProvider {

  implicit val system: ActorSystem
  implicit val materializer: ActorMaterializer
  implicit val execContext: ExecutionContext
  implicit val timeout: Timeout

}

trait DefaultAkkaServiceProvider extends AkkaServiceProvider {
  override implicit val system: ActorSystem = ActorSystem(ServerConfig.systemName, config)
  override implicit val materializer: ActorMaterializer = ActorMaterializer()
  override implicit val timeout: Timeout = Timeout(ServerConfig.defaultTimeout.seconds)
  override implicit val execContext: ExecutionContext = system.dispatcher
}
