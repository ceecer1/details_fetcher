package co.home.intel.util

import com.typesafe.config.ConfigFactory

object SystemConfig {

  val config = ConfigFactory.load()

  case object PostgresqlConfig {
    private val postgreSQLConfig = config.getConfig("postgresql")
    lazy val rdsurl = postgreSQLConfig.getString("rdsurl")
    lazy val rdsusername = postgreSQLConfig.getString("rdsusername")
    lazy val rdspassword = postgreSQLConfig.getString("rdspassword")
  }

}
