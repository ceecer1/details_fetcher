package co.home.intel.domain

import java.time.Instant

case class UserInsertModel(id: Long, name: String, level: Int, is_vip: Int, entry_count: Int, contestId: String, userContest: String)

case class UserReadModel(dbId: Long, id: Long, name: String, level: Int, is_vip: Int, entry_count: Int, contestId: String)

case class ContestInsertModel(id: String,
                              slate_id: Int,
                              name: String,
                              fee: BigDecimal,
                              total_fee: BigDecimal,
                              rake: BigDecimal,
                              prize_pool_value: BigDecimal,
                              entry_count: Int,
                              sport: String,
                              status: String,
                              start_time: Instant,
                              pay_to: String,
                              competition_id: Int,
                              payout_type_key: String,
                              payout_type_desc: String,
                              last_updated: Instant,
                              created: Instant)

case class ContestReadModel(dbId: Long,
                            id: String,
                            slate_id: Int,
                            name: String,
                            fee: BigDecimal,
                            total_fee: BigDecimal,
                            rake: BigDecimal,
                            prize_pool_value: BigDecimal,
                            entry_count: Int,
                            sport: String,
                            status: String,
                            start_time: Instant,
                            pay_to: String,
                            competition_id: Int,
                            payout_type_key: String,
                            payout_type_desc: String,
                            last_updated: Instant)

case class EntryInsertModel(id: Long, fantasy_score: Int, won: Boolean, prize: BigDecimal, userId: Long, name: String, contestId: String)

case class EntryReadModel(dbId: Long, id: Long, won: Boolean, prize: BigDecimal, userId: Long, name: String, contestId: String)


