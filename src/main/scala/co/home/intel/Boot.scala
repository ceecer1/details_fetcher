package co.home.intel

import java.time.Instant
import akka.NotUsed
import akka.actor.Props
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import co.home.intel.client.{ChallengeHttpClient, EntryHttpClient, UserHttpClient}
import co.home.intel.client.ChallengeHttpClient.ChallengeResponse
import co.home.intel.database.postgres.{PGContestDataDBComponent, PGEntryDataDBComponent, PGUserDataDBComponent}
import co.home.intel.domain.{ContestInsertModel, EntryInsertModel, UserInsertModel}
import co.home.intel.handler.ContestInsertHandler
import co.home.intel.handler.ContestInsertHandler.ContestHandlerProtocol.{FetchIncompleteContests, ProcessUpcoming}
import co.home.intel.util.{AkkaServiceProvider, DefaultAkkaServiceProvider, DefaultTransactionProvider, JsonSerialization, Loggable, TransactionProvider}
import co.home.intel.client.ChallengeHttpClient
import ChallengeHttpClient._
import co.home.intel.client.EntryHttpClient.EntryResponse
import co.home.intel.client.UserHttpClient.UserResponse
import co.home.intel.database.Common.{IncompleteContests, InsertContestFunction, InsertEntryFunction, InsertUserFunction}
import co.home.intel.database.postgres.PGEntryDataDBComponent
import co.home.intel.domain.EntryInsertModel
import co.home.intel.handler.ContestInsertHandler
import ContestInsertHandler.ContestHandlerProtocol.{FetchIncompleteContests, FetchPastChallenges, ProcessContests, ProcessUpcoming}
import co.home.intel.util.Loggable

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Boot extends App
  with PGContestDataDBComponent
  with PGEntryDataDBComponent
  with PGUserDataDBComponent
  with EntryHttpClient
  with UserHttpClient
  with ChallengeHttpClient
  with DefaultAkkaServiceProvider
  with DefaultTransactionProvider
  with JsonSerialization with Loggable {

  this: AkkaServiceProvider with TransactionProvider =>

  contestDataDB.initialize()
  userDataDB.initialize()
  entryDataDB.initialize()

  val userModelInsertF: InsertUserFunction = (ls: List[UserInsertModel]) => {
    val rows = ls.map(u => UserInsertModel.unapply(u)).collect { case Some(v) => v }
    userDataDB.insertMany(rows)
  }.unsafeToFuture()

  val contestModelInsertF: InsertContestFunction = (ls: List[ContestInsertModel]) => {
    val rows = ls.map(u => ContestInsertModel.unapply(u)).collect { case Some(v) => v }
    contestDataDB.insertMany(rows)
  }.unsafeToFuture()

  val entryModelInsertF: InsertEntryFunction = (ls: List[EntryInsertModel]) => {
    val rows = ls.map(u => EntryInsertModel.unapply(u)).collect { case Some(v) => v }
    entryDataDB.insertMany(rows)
  }.unsafeToFuture()

  val fetchUpcomingChallengeFunc: String => Future[ChallengeResponse] =
    id => fetchUpcomingChallenges(id)

  val fetchPastChallengeFunc:(String, String) => Future[ChallengeResponse] =
    (id, page) => fetchPastChallenges(id, page)

  val fetchChallengeFunc: String => Future[ChallengeResponse] =
    contestId => fetchChallenges(contestId)

  val fetchIncompleteContests: IncompleteContests = contestDataDB.getIncompleteContests

  val fetchUserFunc: (String, String) => Future[UserResponse] =
    (contestId, page) => fetchUsers(contestId, page)

  val fetchEntryFunc: (String, String) => Future[EntryResponse] =
    (contestId, page) => fetchEntries(contestId, page)

  val contestImporter = system.actorOf(Props(classOf[ContestInsertHandler], materializer, system, fetchUpcomingChallengeFunc,
    fetchPastChallengeFunc, fetchChallengeFunc, fetchUserFunc, fetchEntryFunc, fetchIncompleteContests,
    contestModelInsertF, userModelInsertF, entryModelInsertF))




  /*val contests = List(
    ContestInsertModel("abc", 1, "my", BigDecimal(2), BigDecimal(2), "rake", "prize_pool", 4, "NBA", "Completed"),
    ContestInsertModel("abc", 2, "my", BigDecimal(2), BigDecimal(2), "rake", "prize_pool", 4, "AFL", "Completed")
  )

  contestModelInsert(contests).map {
    c =>
      c match {
        case Right(x) => println(x)
        case Left(ex) => println(ex.getMessage)
      }
  }*/

  /*val pollFunction: () => Future[Option[DrawResponseData]] = () => scoreDataService.poll()*/

  /*val feedPollHandler = system.actorOf(FeedPollHandler.props(pollFunction, inMemoryActorRef), "Feed-Poll-Handler")
  system.scheduler.schedule(1.second, ApiUrlConfig.duration.seconds, feedPollHandler, Poll)*/

  val round6 = List("CZvmAY9","CMEmBcn","ChzmB32","C8MmBZ2","CoImB7y","CIsmBb8","Cl6mAXZ","CyQmB2O","CfpmB4u","CxwmB8M",
    "CYBmCln","CitmCiq","CHgmCc1","Cq3mCf5","CpBmCrB","CJDmCoh","CL2mCkL","C0zmCeA","CN3mCbB","CmYmChQ","C5VmCnD",
    "CWkmCsU","COxmBdo","CbDmALm","CgNmB0L","CrpmB58","C2SmB9m")

  val round5 = List("CHSl2aW","CHhl1ob","CsXl1Lq","CVal1D6","CEil1Pu","CjDl1HU","C6cl1IB","C8pl1Zf","Cmtl12M","CSOl15X",
    "CFRl1b4","C20l1BP","Cd2l1Fn","C99l1Ne","CKHl1Rk","CVWl19P","Cjml1nJ","C8Ml1VK","C6vl1Yj","C9wl18w","C9Sl1Aw",
    "CEOl1E8","CDFl1Mf","CZ2l1Ql","C6kl1Ja","CMyl1Td","C9Yl10F","CFjl137","CgAl1WF","ChRl16x")

  val last_June = List("CzqqddC","Czbp1yz","Czbp1h6","Cyuqcsw","CytqhpI","Cycp1v7","Cxnp8hL","Cxlp9lj","CwxqcrY","CuwqaP7","Cu8p8Nx","CtqqcjF","CswqaOg","Cruqciy","Crpqbl2","CrLqbaJ","CrKp1rL","CrIp8qV","CqOp8m5","Cq3qaVK","Cq0p9n4","Cpxqi5a","CpMqiHZ","Coqqbc9","Cokqa88","Co5qc3o","CnrpZA3","Cndp29S","Cn9qchG","ClNqcqp","ClMp7uU","Cl9pZEm","Ckup8u7","CkTp8PZ","CjSp2Jo","Cj6qhmZ","Ci8qbR2","ChFp89I","Cfqp5kw","Cfgqclz","CfQqhln","CfHqc6T","CdRqa4N","CdMqc0i","CdCqce3","Ccip9hT","CcVp4Iu","CcQp4BK","Ca7qcgj","CZcqbQ4","CWtqhtT","CWUqcfo","CWQqhni","CWHqaQx","CVvp4hi","CVUp4HY","CVOp7JI","CUcqhuJ","CU4p1nX","CTZqbbF","CRxp7tN","CRwqcZb","CRppZzV","CRJqiDj","CQKqhvw","CQKp4AX","CQ3qaRY","CPwqcnn","CP5qc5D","COOqaKp","CNyqa6p","CNxqaLX","CNoqc7J","CNMp7vH","CNCp3QT","CMzp1pQ","CMOqaT8","CLEqhqM","CKEqcmR","CJXp28i","CJMp5p7","CItp276","CImp1xS","CITp9Fu","CHYpZBh","CFwp7rx","CDnqcdV","CD0qdcK","CCaqaNr","CCEqa7J","CByqcX7","CBVqctR","CAip1kl","CAep3PS","CATqhrc","C9OqaUm","C8wqb44","C8vqckD","C7qqi4G","C73qhsO","C6Jp1ou","C52qa5A","C4Gp1i6","C3IqdeH","C30p1j8","C1iqaSp","C1eqa9r","C1Zp0Jw","C1Sqhod","C0Vqdfu","C0FqiPr","C0Ep4zS")

  //system.scheduler.scheduleOnce(50 milliseconds, contestImporter, FetchPastChallenges)
  //system.scheduler.scheduleOnce(50 milliseconds, contestImporter, FetchIncompleteContests)
  //system.scheduler.scheduleOnce(50 milliseconds, contestImporter, ProcessContests(last_June))
  //system.scheduler.scheduleOnce(50 milliseconds, usersImporter, ProcessUserContests(List("CMEmBcn")))
  //system.scheduler.scheduleOnce(50 milliseconds, entriesImporter, ProcessEntryContests(List("CMEmBcn")))

  system.scheduler.schedule(initialDelay = 10 seconds, interval = 1 day) {
    logger.info(s"Importer running for fetching upcoming contests at :${Instant.now}")
    contestImporter ! ProcessUpcoming
  }

  system.scheduler.schedule(initialDelay = 1 hour, interval = 1 day) {
    logger.info(s"Importer running for fetching incomplete contests at :${Instant.now}")
    contestImporter ! FetchIncompleteContests
  }





  //Future.sequence(res).map(_ => system.terminate())*/

 // val bindingFuture = Http().bindAndHandle(routes(inMemoryActorRef), ServerConfig.interface, ServerConfig.port)



}

/**
  * ApplicationStack defines what modules the application requires.
  */
/*
trait ApplicationStack { //extends RestModule {
  //this: DrawStatsDataComponent with AkkaServiceProvider =>
  this: AkkaServiceProvider =>
}

/**
  *  Configure concrete module implementations currently used by http server.
  */
trait ApplicationConfig extends ApplicationStack
  //with DefaultDrawStatsDataComponent
  with DefaultAkkaServiceProvider*/
