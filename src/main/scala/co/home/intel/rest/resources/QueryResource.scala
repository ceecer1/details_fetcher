package co.home.intel.rest.resources

import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import QueryResource.ScoresQuery
import co.home.intel.util.Loggable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import akka.pattern.ask
import akka.util.Timeout
import co.home.intel.database.ContestDataDBComponent
import co.home.intel.util.{JsonSerialization, Loggable}
import co.home.intel.client.ChallengeHttpClient.ChallengeData
import co.home.intel.handler.InMemoryActor.OperationsProtocol.Retrieve

import scala.concurrent.duration._

trait QueryResource extends JsonSerialization with Loggable {

  this: ContestDataDBComponent =>

  implicit val tout = Timeout(5.seconds)

  private lazy val postScore = post & path("query") & entity(as[ScoresQuery])

  def scoreRoute(inMemoryActorRef: ActorRef): Route = postScore { query =>
    complete(getScoreResult(query, inMemoryActorRef))
  } ~
  path("health") {
    get {
      complete("status" -> "running")
    }
  }


  private def getScoreResult(query: ScoresQuery, inMemoryActorRef: ActorRef): Future[Map[Int, Int]] = {



        (inMemoryActorRef ? Retrieve).mapTo[Option[ChallengeData]].map { optPlayerStats =>
        optPlayerStats match {

          case Some(playerStats) => {

            Map.empty[Int, Int]

          }

          case None => Map.empty[Int, Int]
        }
      }
    }

}

object QueryResource {
  case class ScoresQuery(players: Seq[Int],
                         scoringParameter: String)

  case class DrawQuery(MinDrawNo: Int, MaxDrawNo: Int, Product: String = "Powerball", CompanyFilter: Seq[String] = Seq("NSWLotteries"))


}
