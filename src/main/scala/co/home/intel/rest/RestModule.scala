package co.home.intel.rest

import akka.actor.ActorRef
import akka.http.scaladsl.server.Route
import co.home.intel.database.ContestDataDBComponent
import co.home.intel.rest.resources.QueryResource
import co.home.intel.util.AkkaServiceProvider

trait RestModule extends QueryResource {

  this: ContestDataDBComponent with AkkaServiceProvider =>

  def routes(inMemoryActorRef: ActorRef): Route = scoreRoute(inMemoryActorRef)

}
