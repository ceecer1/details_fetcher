package co.home.intel.handler

import akka.actor.{Actor, ActorLogging}
import RowCountTracker.{Completed, CompletedUpcoming, InProgress}

/**
  * Tracks the rows inserted into postgres during streaming
  */
class RowCountTracker extends Actor with ActorLogging {

  implicit val ec = context.dispatcher

  override def receive: Receive = {
    case InProgress(modelType, p) => log.info(s"$modelType Insert/Update in progress with number of records: ${p.get}")
    case Completed(modelType, c) => log.info(s"$modelType Insert/Updated Completed with total records: ${c.get}")
    case CompletedUpcoming(modelType) => log.info(s"$modelType Insert/Updated Completed")
  }

}

object RowCountTracker {
  case class InProgress(modelType: String, counter: Counter)
  case class Completed(modelType: String, count: Counter)
  case class CompletedUpcoming(modelType: String)
}


