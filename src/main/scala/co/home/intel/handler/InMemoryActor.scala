package co.home.intel.handler

import akka.actor.{Actor, Props}
import co.home.intel.client.ChallengeHttpClient.ChallengeData
import InMemoryActor.OperationsProtocol._

/**
  * This actor serves as inmemory for persisting the response data received from hitting the url
  */
class InMemoryActor extends Actor {
  def receive = active(None)

  def active(sportsData: Option[ChallengeData]): Receive = {
    case Save(data) =>
      context become active(data)

    case Retrieve =>
      sender() ! sportsData
  }
}

object InMemoryActor {
  def props(): Props = Props(new InMemoryActor())

  object OperationsProtocol {
    case class Save(data: Option[ChallengeData])
    case object Retrieve
  }
}

