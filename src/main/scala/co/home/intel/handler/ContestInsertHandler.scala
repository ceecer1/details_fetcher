package co.home.intel.handler

import java.time.Instant
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Broadcast, Flow, Keep, Sink, Source}
import co.home.intel.domain.ContestInsertModel
import co.home.intel.client.ChallengeHttpClient.{ChallengeResponse, ChallengeResult, UpcomingResult}
import co.home.intel.client.EntryHttpClient.EntryResponse
import co.home.intel.client.UserHttpClient.UserResponse
import co.home.intel.database.Common.{IncompleteContests, InsertContestFunction, InsertEntryFunction, InsertUserFunction}
import ContestInsertHandler.ContestHandlerProtocol.{EntryBranchProcessCompleted, FetchIncompleteContests, FetchPastChallenges, GetPastContests, ProcessContests, ProcessUpcoming, UserBranchProcessCompleted}
import EntryInsertHandler.EntryHandlerProtocol.ProcessEntryContests
import RowCountTracker.{Completed, CompletedUpcoming, InProgress}
import UserInsertHandler.UserHandlerProtocol.ProcessUserContests

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.util.{Failure, Success}

class ContestInsertHandler(implicit mat: ActorMaterializer, system: ActorSystem,
                           fetchUpcomingChallenges: String => Future[ChallengeResponse],
                           fetchPastChallenges: (String, String) => Future[ChallengeResponse],
                           fetchChallengeFunc: String => Future[ChallengeResponse],
                           fetchUserFunc: (String, String) => Future[UserResponse],
                           fetchEntryFunc: (String, String) => Future[EntryResponse],
                           fetchIncompleteContests: IncompleteContests,
                           insertContestFunction: InsertContestFunction,
                           userInsertFunction: InsertUserFunction,
                           entryInsertFunction: InsertEntryFunction) extends Actor

  with ActorLogging {

  implicit val ec = system.dispatcher

  val counterFlow: Flow[Option[ContestInsertModel], Option[ContestInsertModel], Counter] = CounterFlow[Option[ContestInsertModel]]
  val listCounterFlow: Flow[List[ContestInsertModel], List[ContestInsertModel], Counter] = CounterFlow[List[ContestInsertModel]]
  val tracker = context.actorOf(Props(classOf[RowCountTracker]))
  val usersImporter = context.actorOf(Props(classOf[UserInsertHandler], mat, system, self, fetchUserFunc, userInsertFunction))
  val entriesImporter = context.actorOf(Props(classOf[EntryInsertHandler], mat, system, self, fetchEntryFunc, entryInsertFunction))

  var contestPages: List[String] = (1 to 10).toList.map(_.toString)//List.empty[String]
  //var contestIds: List[String] = List.empty[String]

  def receive: Receive = {
    case FetchPastChallenges =>
      contestPages match {
        case h :: t => {
          contestPages = t
          log.info(s"Getting contests from page: ${h}")
          self ! GetPastContests(h)
        }
        case Nil => log.info("No more past contests to process, Done.")
      }

    case pp: GetPastContests =>
      Source.fromFuture(fetchPastChallenges("Dummy param to defer evaluation", pp.page))
        .map {
          response =>
            response match {
              case response: UpcomingResult =>
                val contestSeq = response.data
                val idOptSeq = contestSeq.map(_.id)
                val ids = idOptSeq.map{case Some(x) => x}.toList
                self ! ProcessContests(ids)
              case _ => Nil
            }
        }
        //.viaMat(listCounterFlow)(Keep.right)
        //.mapAsync(2)(insertContestFunction)
        .toMat(Sink.ignore)(Keep.both).run()

      /*val cancellable = context.system.scheduler.schedule(1 second, 5 seconds, tracker, InProgress("Contests", counter))
      d.onComplete { _ =>
        tracker ! Completed("Contests", counter)
        cancellable.cancel()
        usersImporter ! ProcessUserContests(contestIds)
      }*/



    case FetchIncompleteContests =>
      fetchIncompleteContests.unsafeToFuture().map {
        case Right(ids) =>
          if (ids.isEmpty) {
            log.info("No more incomplete contests")
          } else {
            self ! ProcessContests(ids)
          }
        case Left(ex) => log.error(s"Error encountered while retrieving incomplete contests with msg: ${ex.getMessage}")
      }

    case ProcessUpcoming =>
      val (_, res) = Source.fromFuture(fetchUpcomingChallenges("Dummy param to defer evaluation"))
        .map {
          response =>
            response match {
              case response: UpcomingResult =>
                val contestSeq = response.data
                contestSeq.map(_.id)
              case _ => Nil
            }
        }.toMat(Sink.head)(Keep.both).run()

      res.onComplete {
        case Success(v) => {

          self ! ProcessContests(v.map{case Some(x) => x}.toList)
        }
        case Failure(f) => log.error(s"Error encountered while extracting upcoming contests with msg: ${f.getMessage}")
      }

    case pc: ProcessContests =>
      usersImporter ! ProcessUserContests(pc.contestList)
      entriesImporter ! ProcessEntryContests(pc.contestList)
      val (counter, d) = Source(pc.contestList)
        .mapAsync(5)(fetchChallengeFunc)
        .map { response =>
          response match {
            case response: ChallengeResult =>
              val data = response.data
              Some(ContestInsertModel(data.id.getOrElse("no_id"), data.slate_id.getOrElse(-1), data.name.getOrElse("no name"), data.fee.getOrElse(BigDecimal(0)),
                data.total_fee.getOrElse(BigDecimal(0)), data.rake.getOrElse(BigDecimal(0)),
                data.prize_pool_value.getOrElse(BigDecimal(0)), data.entry_count.getOrElse(-1), data.competition_type.short_name.getOrElse("na"), data.status.name.getOrElse("none"),
                data.start_time, data.pay_to.getOrElse("na"), data.competition_id.getOrElse(-1), data.payout_type.key.getOrElse("na"), data.payout_type.description.getOrElse("na"), Instant.now, Instant.now))

            case _ => None
          }

        }.viaMat(counterFlow)(Keep.right)
        .grouped(20)
        .map(x => x.map { case Some(x) => x }.toList)
        .mapAsync(2)(insertContestFunction)
        .toMat(Sink.ignore)(Keep.both).run()

      val cancellable = context.system.scheduler.schedule(1 second, 5 seconds, tracker, InProgress("Contests", counter))
      d.onComplete { _ =>
        tracker ! Completed("Contests", counter)
        cancellable.cancel()
      }

    case UserBranchProcessCompleted =>
      log.info("Users processing Completed!")

    case EntryBranchProcessCompleted =>
      /*if(contestPages.length > 0) {
        self ! FetchPastChallenges
      } else {*/
        log.info("Entries processing Completed")
      //}
  }


}

object ContestInsertHandler {

  case object ContestHandlerProtocol {

    case class ProcessContests(contestList: List[String])

    case object FetchIncompleteContests

    case object ProcessUpcoming

    case object FetchPastChallenges

    case class GetPastContests(page: String)

    case object UserBranchProcessCompleted
    case object EntryBranchProcessCompleted

  }

}
