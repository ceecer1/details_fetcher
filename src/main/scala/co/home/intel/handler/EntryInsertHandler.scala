package co.home.intel.handler

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink, Source}
import co.home.intel.handler.ContestInsertHandler.ContestHandlerProtocol.EntryBranchProcessCompleted
import co.home.intel.client.EntryHttpClient.{EntryResponse, EntryResult}
import co.home.intel.database.Common.InsertEntryFunction
import ContestInsertHandler.ContestHandlerProtocol.EntryBranchProcessCompleted
import EntryInsertHandler.EntryHandlerProtocol.{ProcessEntries, ProcessEntryCompleted, ProcessEntryContests, QueryPages}
import co.home.intel.domain.EntryInsertModel

import scala.concurrent.Future
import scala.util.{Failure, Success}

class EntryInsertHandler(implicit mat: ActorMaterializer, system: ActorSystem, parent: ActorRef,
                        fetchEntryFunc: (String, String) => Future[EntryResponse], insertFunction: InsertEntryFunction) extends Actor

  with ActorLogging {

  var contests: List[String] = List.empty[String]
  var pages: List[String] = List.empty[String]

  implicit val ec = system.dispatcher

  def receive: Receive = {
    case pc: ProcessEntries =>
      Source(pc.pageList)
        .mapAsync(2)(s => fetchEntryFunc(pc.contestId, s))
        .map { response =>
          response match {
            case response: EntryResult =>
              //val meta = response.meta
              val userSeq = response.data.map {
                d =>
                  Some(EntryInsertModel(d.id, d.fantasy_score.getOrElse(-1), d.won.getOrElse(false),
                    d.prize.value.getOrElse(BigDecimal(0)), d.user.id.getOrElse(1L), d.user.name.getOrElse("na"), pc.contestId))
              }
              userSeq
            case _ => Nil
          }
        }.grouped(2)
        .map(x => x.flatten.map{case Some(x) => x}.toList)
        .mapAsync(2)(insertFunction)
        .runWith(Sink.ignore)
        .map { _ =>
          pages = Nil
          contests match {
            case h :: t => {
              contests = t
              self ! QueryPages(h)
            }
            case Nil => self ! ProcessEntryCompleted("no more entries to process")
          }
        }

    case c: ProcessEntryCompleted =>
      parent ! EntryBranchProcessCompleted
      log.info(s"${c.msg}")

    case puc: ProcessEntryContests =>
      contests = puc.ids
      contests match {
        case h :: t => {
          contests = t
          self ! QueryPages(h)
        }
        case Nil => self ! ProcessEntryCompleted(s"no contests to process during entering processing users for contestList ${contests}")
      }

    case qp: QueryPages =>
      val (_, res) = Source.single(qp.contestId)
        .mapAsync(1)(s => fetchEntryFunc(s, "1"))
        .map { response =>
          response match {
            case response: EntryResult =>
              response.meta.pagination.last_page
            case _ => 1
          }
        }.toMat(Sink.head)(Keep.both).run()
      res.onComplete {
        case Success(v) => {
          pages = (1 to v).toList.map(_.toString)
          self ! ProcessEntries(qp.contestId, pages)
        }
        case Failure(f) => throw new RuntimeException
      }

  }



}

object EntryInsertHandler {
  case object EntryHandlerProtocol {
    case class ProcessEntryContests(ids: List[String])
    case class QueryPages(contestId: String)
    case class ProcessEntries(contestId: String, pageList: List[String])
    case class ProcessEntryCompleted(msg: String)
  }
}
