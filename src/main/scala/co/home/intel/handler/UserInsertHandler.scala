package co.home.intel.handler

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, Sink, Source}
import co.home.intel.handler.ContestInsertHandler.ContestHandlerProtocol.UserBranchProcessCompleted
import co.home.intel.client.UserHttpClient.{UserResponse, UserResult}
import co.home.intel.database.Common.InsertUserFunction
import co.home.intel.domain.ContestInsertModel
import ContestInsertHandler.ContestHandlerProtocol.UserBranchProcessCompleted
import UserInsertHandler.UserHandlerProtocol.{ProcessUserCompleted, ProcessUserContests, ProcessUsers, QueryPages}
import co.home.intel.domain.UserInsertModel

import scala.concurrent.Future
import scala.util.{Failure, Success}

class UserInsertHandler(implicit mat: ActorMaterializer, system: ActorSystem, parent: ActorRef,
                        fetchUserFunc: (String, String) => Future[UserResponse], insertFunction: InsertUserFunction) extends Actor

  with ActorLogging {

  var contests: List[String] = List.empty[String]
  var pages: List[String] = List.empty[String]

  implicit val ec = system.dispatcher

  def receive: Receive = {
    case pc: ProcessUsers =>
      Source(pc.pageList)
        .mapAsync(2)(s => fetchUserFunc(pc.contestId, s))
        .map { response =>
          response match {
            case response: UserResult =>
              //val meta = response.meta
              val userSeq = response.data.map {
                d =>
                  Some(UserInsertModel(d.id, d.name.getOrElse("na"), d.level.getOrElse(-1), d.is_vip.getOrElse(-1),
                    d.entry_count.getOrElse(-1), pc.contestId, s"${d.name}_${pc.contestId}"))
              }
              userSeq
            case _ => Nil
          }
        }.grouped(2)
        .map(x => x.flatten.map{case Some(x) => x}.toList)
        .mapAsync(2)(insertFunction)
        .runWith(Sink.ignore)
        .map { _ =>
          pages = Nil
          contests match {
            case h :: t => {
              contests = t
              self ! QueryPages(h)
            }
            case Nil => self ! ProcessUserCompleted("no more users to process")
          }
        }

    case c: ProcessUserCompleted =>
      parent ! UserBranchProcessCompleted
      log.info(s"${c.msg}")

    case puc: ProcessUserContests =>
      contests = puc.ids
      contests match {
        case h :: t => {
          contests = t
          self ! QueryPages(h)
        }
        case Nil => self ! ProcessUserCompleted(s"no more users to process during entering processing users for contestList ${contests}")
      }

    case qp: QueryPages =>
      val (_, res) = Source.single(qp.contestId)
        .mapAsync(1)(s => fetchUserFunc(s, "1"))
        .map { response =>
          response match {
            case response: UserResult =>
              response.meta.pagination.last_page
            case _ => 1
          }
        }.toMat(Sink.head)(Keep.both).run()
      res.onComplete {
        case Success(v) => {
          pages = (1 to v).toList.map(_.toString)
          self ! ProcessUsers(qp.contestId, pages)
        }
        case Failure(f) => throw new RuntimeException
      }


  }



}

object UserInsertHandler {
  case object UserHandlerProtocol {
    case class ProcessUserContests(ids: List[String])
    case class QueryPages(contestId: String)
    case class ProcessUsers(contestId: String, pageList: List[String])
    case class ProcessUserCompleted(msg: String)
  }
}
