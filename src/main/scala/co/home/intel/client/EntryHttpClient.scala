package co.home.intel.client

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.util.ByteString
import co.home.intel.util.Config.ApiUrlConfig
import co.home.intel.util.{AkkaServiceProvider, JsonSerialization, Loggable}
import co.home.intel.util.Config.ApiUrlConfig
import co.home.intel.util.Loggable

import scala.concurrent.Future
import scala.concurrent.duration._


/**
  * Responsible for doing the http request to the given url and gather a response
  */
trait EntryHttpClient extends JsonSerialization with Loggable {

  this: AkkaServiceProvider =>

  import EntryHttpClient._

  def fetchEntries(contestId: String, page: String = "1"): Future[EntryResponse] = {
    val httpRequest = HttpRequest(uri = s"${ApiUrlConfig.challengeURL}$contestId/entries?page=$page")
    val httpResponse: Future[HttpResponse] = Http().singleRequest(httpRequest)

    httpResponse flatMap { response =>
      // toStrict to enforce all data be loaded into memory from the connection
      val strictEntity = response.entity.toStrict(50.seconds)
      strictEntity flatMap { e =>
        val responseStr = e.dataBytes
          .runFold(ByteString.empty) { case (acc, b) => acc ++ b }
        response.status match {
          case OK => responseStr map (s => serialization.read[EntryResult](s.utf8String))
          case Forbidden => responseStr.map(s => EntryForbidden(s.utf8String))
          case TooManyRequests => responseStr.map(s => EntryTooManyRequests(s.utf8String))
          case NotFound => responseStr.map(s => EntryNotFound(s.utf8String))
          case _ => responseStr.map(s => EntryGenericError(s.utf8String))
        }
      }
    }
  }

}

object EntryHttpClient {
  trait EntryResponse

  trait EntryError extends EntryResponse

  //Exceptional API call conditions
  case class EntryGenericError(message: String) extends EntryError

  case class EntryNotFound(message: String) extends EntryError

  case class EntryTooManyRequests(message: String) extends EntryError

  case class EntryForbidden(message: String) extends EntryError


  //partial structure for sports feed data, which is derived looking at json feed response
  case class EntryData(userResult: EntryResult) extends EntryResponse
  case class UserDetail(id: Option[Long], name: Option[String])
  case class Prize(value: Option[BigDecimal])

  case class Entry(id: Long,
                   fantasy_score: Option[Int],
                   won: Option[Boolean],
                   prize: Prize,
                   user: UserDetail)

  case class PaginationDetails(has_next_page: Boolean, last_page: Int, next_page_url: Option[String], total: Int)
  case class Pagination(pagination: PaginationDetails)
  case class EntryResult(data: Seq[Entry], meta: Pagination) extends EntryResponse

}

