package co.home.intel.client

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.util.ByteString
import co.home.intel.util.Config.ApiUrlConfig
import co.home.intel.util.{AkkaServiceProvider, JsonSerialization, Loggable}
import co.home.intel.util.Config.ApiUrlConfig
import co.home.intel.util.Loggable

import scala.concurrent.Future
import scala.concurrent.duration._


/**
  * Responsible for doing the http request to the given url and gather a response
  */
trait UserHttpClient extends JsonSerialization with Loggable {

  this: AkkaServiceProvider =>

  import UserHttpClient._

  def fetchUsers(contestId: String, page: String = "1"): Future[UserResponse] = {
    val httpRequest = HttpRequest(uri = s"${ApiUrlConfig.challengeURL}$contestId/users?page=$page")
    val httpResponse: Future[HttpResponse] = Http().singleRequest(httpRequest)

    httpResponse flatMap { response =>
      // toStrict to enforce all data be loaded into memory from the connection
      val strictEntity = response.entity.toStrict(50.seconds)
      strictEntity flatMap { e =>
        val responseStr = e.dataBytes
          .runFold(ByteString.empty) { case (acc, b) => acc ++ b }
        response.status match {
          case OK => responseStr map (s => serialization.read[UserResult](s.utf8String))
          case Forbidden => responseStr.map(s => UserForbidden(s.utf8String))
          case TooManyRequests => responseStr.map(s => UserTooManyRequests(s.utf8String))
          case NotFound => responseStr.map(s => UserNotFound(s.utf8String))
          case _ => responseStr.map(s => UserGenericError(s.utf8String))
        }
      }
    }
  }

}

object UserHttpClient {
  trait UserResponse

  trait UserError extends UserResponse

  //Exceptional API call conditions
  case class UserGenericError(message: String) extends UserError

  case class UserNotFound(message: String) extends UserError

  case class UserTooManyRequests(message: String) extends UserError

  case class UserForbidden(message: String) extends UserError


  //partial structure for sports feed data, which is derived looking at json feed response
  case class UserData(userResult: UserResult) extends UserResponse

  case class User(id: Long,
                  name: Option[String],
                  level: Option[Int],
                  is_vip: Option[Int],
                  entry_count: Option[Int])

  case class PaginationDetails(has_next_page: Boolean, last_page: Int, next_page_url: Option[String])
  case class Pagination(pagination: PaginationDetails)
  case class UserResult(data: Seq[User], meta: Pagination) extends UserResponse
}

