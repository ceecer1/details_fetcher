package co.home.intel.client

import java.time.Instant
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.util.ByteString
import co.home.intel.util.Loggable
import akka.http.scaladsl.model.StatusCodes._
import co.home.intel.util.Config.ApiUrlConfig
import co.home.intel.util.{AkkaServiceProvider, JsonSerialization, Loggable}
import co.home.intel.util.Config.ApiUrlConfig

import scala.concurrent.Future
import scala.concurrent.duration._


/**
  * Responsible for doing the http request to the given url and gather a response
  */
trait ChallengeHttpClient extends JsonSerialization with Loggable {

  this: AkkaServiceProvider =>

  import ChallengeHttpClient._

  def fetchChallenges(contestId: String): Future[ChallengeResponse] = {
    val httpRequest = HttpRequest(uri = s"${ApiUrlConfig.challengeURL}$contestId")
    val httpResponse: Future[HttpResponse] = Http().singleRequest(httpRequest)

    httpResponse flatMap { response =>
      // toStrict to enforce all data be loaded into memory from the connection
      val strictEntity = response.entity.toStrict(50.seconds)
      strictEntity flatMap { e =>
        val responseStr = e.dataBytes
          .runFold(ByteString.empty) { case (acc, b) => acc ++ b }
        response.status match {
          case OK => responseStr map (s => serialization.read[ChallengeResult](s.utf8String))
          case Forbidden => responseStr.map(s => ChallengeForbidden(s.utf8String))
          case TooManyRequests => responseStr.map(s => ChallengeTooManyRequests(s.utf8String))
          case NotFound => responseStr.map(s => ChallengeNotFound(s.utf8String))
          case _ => responseStr.map(s => ChallengeGenericError(s.utf8String))
        }
      }
    }
  }

  def fetchUpcomingChallenges(id: String = "Dummy Parameter to defer evaluation"): Future[ChallengeResponse] = {
    val httpRequest = HttpRequest(uri = s"${ApiUrlConfig.upcomingURL}")
    val httpResponse: Future[HttpResponse] = Http().singleRequest(httpRequest)

    httpResponse flatMap { response =>
      // toStrict to enforce all data be loaded into memory from the connection
      val strictEntity = response.entity.toStrict(50.seconds)
      strictEntity flatMap { e =>
        val responseStr = e.dataBytes
          .runFold(ByteString.empty) { case (acc, b) => acc ++ b }
        response.status match {
          case OK => responseStr map (s => serialization.read[UpcomingResult](s.utf8String))
          case Forbidden => responseStr.map(s => ChallengeForbidden(s.utf8String))
          case TooManyRequests => responseStr.map(s => ChallengeTooManyRequests(s.utf8String))
          case NotFound => responseStr.map(s => ChallengeNotFound(s.utf8String))
          case _ => responseStr.map(s => ChallengeGenericError(s.utf8String))
        }
      }
    }
  }

  def fetchPastChallenges(id: String = "Dummy Parameter to defer evaluation", page: String): Future[ChallengeResponse] = {
    val url = s"${ApiUrlConfig.pastURL}$page"
    logger.info(url)
    val httpRequest = HttpRequest(uri = url)
    val httpResponse: Future[HttpResponse] = Http().singleRequest(httpRequest)

    httpResponse flatMap { response =>
      // toStrict to enforce all data be loaded into memory from the connection
      val strictEntity = response.entity.toStrict(50.seconds)
      strictEntity flatMap { e =>
        val responseStr = e.dataBytes
          .runFold(ByteString.empty) { case (acc, b) => acc ++ b }
        response.status match {
          case OK => responseStr map (s => serialization.read[UpcomingResult](s.utf8String))
          case Forbidden => responseStr.map(s => ChallengeForbidden(s.utf8String))
          case TooManyRequests => responseStr.map(s => ChallengeTooManyRequests(s.utf8String))
          case NotFound => responseStr.map(s => ChallengeNotFound(s.utf8String))
          case _ => responseStr.map(s => ChallengeGenericError(s.utf8String))
        }
      }
    }
  }

}

object ChallengeHttpClient {

  trait ChallengeResponse

  trait ChallengeError extends ChallengeResponse

  //Exceptional API call conditions
  case class ChallengeGenericError(message: String) extends ChallengeError

  case class ChallengeNotFound(message: String) extends ChallengeError

  case class ChallengeTooManyRequests(message: String) extends ChallengeError

  case class ChallengeForbidden(message: String) extends ChallengeError


  //partial structure for sports feed data, which is derived looking at json feed response
  case class ChallengeData(challengeResult: ChallengeResult) extends ChallengeResponse

  case class Status(name: Option[String])
  case class CompetitionType(id: Option[Int], name: Option[String], short_name: Option[String])
  case class PayoutType(key: Option[String], description: Option[String])
  case class Challenge(id: Option[String],
                       slate_id: Option[Int],
                       name: Option[String],
                       fee: Option[BigDecimal],
                       total_fee: Option[BigDecimal],
                       rake: Option[BigDecimal],
                       prize_pool_value: Option[BigDecimal],
                       status: Status,
                       entry_count: Option[Int],
                       start_time: Instant,
                       pay_to: Option[String],
                       competition_id: Option[Int],
                       competition_type: CompetitionType,
                       payout_type: PayoutType)
  case class ChallengeResult(data: Challenge) extends ChallengeResponse

  case class UpcomingResult(data: Seq[Challenge]) extends ChallengeResponse


}