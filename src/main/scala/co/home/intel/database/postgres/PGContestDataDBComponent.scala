package co.home.intel.database.postgres

import java.time.Instant
import scala.language.higherKinds
import cats.effect.IO
import co.home.intel.database.Common.ContestInfo
import doobie.implicits._
import cats.implicits._
import co.home.intel.database.ContestDataDBComponent
import co.home.intel.domain.ContestReadModel
import co.home.intel.util.TransactionProvider
import doobie.util.update.{Update, Update0}


trait PGContestDataDBComponent extends ContestDataDBComponent {

  this: TransactionProvider =>

  override val contestDataDB: ContestDataDB = new PGContestDataDB

  private class PGContestDataDB extends ContestDataDB {
    override def initialize(): Unit = {
      val create: Update0 =
        sql"""
              CREATE TABLE IF NOT EXISTS CONTESTS (
                DBID SERIAL PRIMARY KEY,
                ID VARCHAR UNIQUE,
                SLATE_ID INTEGER,
                NAME VARCHAR,
                FEE DECIMAL,
                TOTAL_FEE DECIMAL(13, 3),
                RAKE DECIMAL(13, 3),
                PRIZE_POOL_VALUE DECIMAL(13, 3),
                ENTRY_COUNT INTEGER,
                SPORT VARCHAR(10),
                STATUS VARCHAR(40),
                START_TIME TIMESTAMP,
                PAY_TO VARCHAR(20),
                COMPETITION_ID INTEGER,
                PAYOUT_TYPE_KEY VARCHAR(200),
                PAYOUT_TYPE_DESC VARCHAR(200),
                LAST_UPDATED TIMESTAMP,
                CREATED TIMESTAMP DEFAULT NOW()
              )
            """.update
      create.run.transact(xa).unsafeToFuture()
      ()
    }

    override def insertMany(ps: List[ContestInfo]): IO[Either[Exception, Int]] = {
      val sql = "insert into contests (id, slate_id, name, fee, total_fee, rake, prize_pool_value, entry_count, sport, " +
        "status, start_time, pay_to, competition_id, payout_type_key, payout_type_desc, last_updated, created) " +
        "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT (id) DO UPDATE SET (status, " +
        "entry_count, prize_pool_value, last_updated) = (EXCLUDED.status, EXCLUDED.entry_count, EXCLUDED.prize_pool_value, EXCLUDED.last_updated)"
      Update[ContestInfo](sql).updateMany(ps).transact(xa).attemptSql
    }

    override def getIncompleteContests: IO[Either[Exception, List[String]]] = {
      sql"""
           SELECT ID FROM contests WHERE status != 'Completed' AND status != 'Cancelled' AND start_time >= '2019-07-22 00:00:00' ;
     """.query[String].to[List].transact(xa).attemptSql
    }
  }

}
