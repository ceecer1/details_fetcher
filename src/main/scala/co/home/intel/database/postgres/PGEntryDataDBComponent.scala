package co.home.intel.database.postgres

import cats.effect.IO
import cats.implicits._
import co.home.intel.util.TransactionProvider
import co.home.intel.database.Common.{ContestInfo, EntryInfo}
import co.home.intel.database.EntryDataDBComponent
import co.home.intel.database.ContestDataDBComponent
import doobie.implicits._
import doobie.util.update.{Update, Update0}

import scala.language.higherKinds


trait PGEntryDataDBComponent extends EntryDataDBComponent {

  this: TransactionProvider =>

  override val entryDataDB: EntryDataDB = new PGEntryDataDB

  private class PGEntryDataDB extends EntryDataDB {
    override def initialize(): Unit = {
      val create: Update0 =
        sql"""
              CREATE TABLE IF NOT EXISTS ENTRIES (
                DBID SERIAL PRIMARY KEY,
                ID INTEGER UNIQUE,
                FANTASY_SCORE INTEGER,
                WON BOOLEAN,
                PRIZE DECIMAL,
                USER_ID INTEGER,
                NAME VARCHAR,
                CONTEST_ID VARCHAR
              )
            """.update
      create.run.transact(xa).unsafeToFuture()
      ()
    }

    override def insertMany(ps: List[EntryInfo]): IO[Either[Exception, Int]] = {
      val sql = "insert into entries (id, fantasy_score, won, prize, user_id, name, contest_id) " +
        "values (?, ?, ?, ?, ?, ?, ?) ON CONFLICT (id) DO UPDATE SET (fantasy_score, " +
        "won, prize) = (EXCLUDED.fantasy_score, EXCLUDED.won, EXCLUDED.prize)"
      Update[EntryInfo](sql).updateMany(ps).transact(xa).attemptSql
    }
  }

}
