package co.home.intel.database.postgres

import cats.effect.IO
import cats.implicits._
import co.home.intel.util.TransactionProvider
import co.home.intel.database.Common.{ContestInfo, UserInfo}
import co.home.intel.database.UserDataDBComponent
import co.home.intel.database.ContestDataDBComponent
import doobie.implicits._
import doobie.util.update.{Update, Update0}

import scala.language.higherKinds


trait PGUserDataDBComponent extends UserDataDBComponent {

  this: TransactionProvider =>

  override val userDataDB: UserDataDB = new PGUserDataDB

  private class PGUserDataDB extends UserDataDB {
    override def initialize(): Unit = {
      val create: Update0 =
        sql"""
              CREATE TABLE IF NOT EXISTS USERS (
                DBID SERIAL PRIMARY KEY,
                ID INTEGER,
                NAME VARCHAR,
                LEVEL SMALLINT,
                IS_VIP SMALLINT,
                ENTRY_COUNT INTEGER,
                CONTEST_ID VARCHAR,
                USER_CONTEST VARCHAR UNIQUE
              )
            """.update
      create.run.transact(xa).unsafeToFuture()
      ()
    }

    override def insertMany(ps: List[UserInfo]): IO[Either[Exception, Int]] = {
      val sql = "insert into users (id, name, level, is_vip, entry_count, contest_id, user_contest) " +
        "values (?, ?, ?, ?, ?, ?, ?) ON CONFLICT (user_contest) DO UPDATE SET (level, " +
        "is_vip, entry_count) = (EXCLUDED.level, EXCLUDED.is_vip, EXCLUDED.entry_count)"
      Update[UserInfo](sql).updateMany(ps).transact(xa).attemptSql
    }
  }

}
