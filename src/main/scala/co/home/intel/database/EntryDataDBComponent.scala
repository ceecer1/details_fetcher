package co.home.intel.database

import cats.effect.IO
import Common.{ContestInfo, EntryInfo}

trait EntryDataDBComponent {

  val entryDataDB: EntryDataDB

  trait EntryDataDB {
    def initialize(): Unit
    def insertMany(ps: List[EntryInfo]): IO[Either[Exception, Int]]
  }

}
