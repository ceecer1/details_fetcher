package co.home.intel.database

import cats.effect.IO
import Common.ContestInfo
import co.home.intel.domain.ContestReadModel

import scala.concurrent.ExecutionContext

trait ContestDataDBComponent {

  val contestDataDB: ContestDataDB

  trait ContestDataDB {
    def initialize(): Unit
    def insertMany(ps: List[ContestInfo]): IO[Either[Exception, Int]]
    def getIncompleteContests: IO[Either[Exception, List[String]]]
  }

}
