package co.home.intel.database

import java.time.Instant
import cats.effect.IO
import co.home.intel.domain.{ContestInsertModel, EntryInsertModel, UserInsertModel}
import co.home.intel.domain.EntryInsertModel

import scala.concurrent.Future

object Common {

  type UserInfo = (Long, String, Int, Int, Int, String, String)
  type EntryInfo = (Long, Int, Boolean, BigDecimal, Long, String, String)
  type ContestInfo = (String, Int, String, BigDecimal, BigDecimal, BigDecimal, BigDecimal, Int, String, String,
    Instant, String, Int, String, String, Instant, Instant)

  ////start_time: Instant,
  //            //                              pay_to: String,
  //            //                              competition_id: Int,
  //            //                              payout_type_key: String,
  //            //                              payout_type_desc: String


  type InsertUserFunction = List[UserInsertModel] => Future[Either[Exception, Int]]
  type InsertEntryFunction = List[EntryInsertModel] => Future[Either[Exception, Int]]
  type InsertContestFunction = List[ContestInsertModel] => Future[Either[Exception, Int]]
  type IncompleteContests = IO[Either[Exception, List[String]]]

}
