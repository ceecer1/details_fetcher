package co.home.intel.database

import cats.effect.IO
import Common.{ContestInfo, UserInfo}

trait UserDataDBComponent {

  val userDataDB: UserDataDB

  trait UserDataDB {
    def initialize(): Unit
    def insertMany(ps: List[UserInfo]): IO[Either[Exception, Int]]
  }

}
